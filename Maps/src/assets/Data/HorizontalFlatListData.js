var horizontalFlatListData=[
    {
        hour:"0 AM",
        status:"rainy",
        degrees: 26
    },
    {
        hour:"1 AM",
        status:"rainy",
        degrees: 27
    },
    {
        hour:"2 AM",
        status:"rainy",
        degrees: 28
    },
    {
        hour:"3 AM",
        status:"rainy",
        degrees: 29
    },
    {
        hour:"4 AM",
        status:"rainy",
        degrees: 20
    },
    {
        hour:"5 AM",
        status:"rainy",
        degrees: 21
    },
    {
        hour:"6 AM",
        status:"rainy",
        degrees: 26
    }
];

export {horizontalFlatListData};