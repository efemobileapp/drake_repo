var Gas_Station_List_data=[
    {
        name:"Petrolimex 17",
        location:"Lô 93-94 Nguyễn Hữu Thọ, Q. Cẩm Lệ. Mở cửa tới 9:00 PM.",
        distance:"2.53 KM",
        time:"5 phút",
    },
    {
        name:"Petrolimex 18",
        location:"Lô 93-94 Nguyễn Hữu Thọ, Q. Cẩm Lệ. Mở cửa tới 9:00 PM.",
        distance:"2.53 KM",
        time:"5 phút",
    },
    {
        name:"Petrolimex 19",
        location:"Lô 93-94 Nguyễn Hữu Thọ, Q. Cẩm Lệ. Mở cửa tới 9:00 PM.",
        distance:"2.53 KM",
        time:"5 phút",
    }
]

export {Gas_Station_List_data};