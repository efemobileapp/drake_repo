import {StyleSheet} from 'react-native';

const styles=StyleSheet.create({
    View_city:{
        position:'absolute',
        bottom:310,
        left:15,
    },
    Text_vtht:{
        fontSize:18,
        fontWeight:'bold',
        color: '#FFFFFF',
        margin:3,
        marginLeft:15
    },
    Text_city:{
        fontSize:30,
        fontWeight:'bold',
        color: '#FFFFFF',
        margin:3,
        marginLeft:15
    },
    Text_streetname:{
        fontSize:20,
        color: '#FFFFFF',
        margin:3,
        marginLeft:15
    },
})

export default styles;