import React, {Component} from 'react';
import {
    View, Text
} from 'react-native'
import styles from './styles';
export default class Geocoder extends Component{
    render(){
        return(
            <View style={styles.View_city}>
                <Text style={styles.Text_vtht}>Vị trí hiện tại</Text>
                <Text style={styles.Text_city}>TP. {this.props.mylocation.city_name}</Text>
                <Text style={styles.Text_streetname}>Đường {this.props.mylocation.street_name}</Text>
            </View>
    );}
}
