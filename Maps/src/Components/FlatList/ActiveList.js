import React, {Component} from 'react';
import {
    View, Image, Text, TouchableOpacity,
} from 'react-native';
import styles from './styles';

export default class GasStationList extends Component{ //Lớp tạo giao diện FlatList

    constructor(props){super(props)}

    update=()=>{
        var lat=this.props.item.geometry.location.lat;
        var long=this.props.item.geometry.location.lng;
        this.props.updateDistance(lat,long);
    }

    render(){
        return(
            
            <View style={{flex:1}}>
                <View style={styles.View_FlatList_Item} key={this.props.index}>
                    <View style={styles.container}>
                        <View style={styles.View_name}>
                            <Image source={require('../../assets/images/gas-server-32.png')}/>
                            <Text numberOfLines={1} style={styles.Text_name}>{this.props.item.name}</Text>
                        </View>
                        <View style={styles.View_location}>
                            <Text style={styles.Text_Location}>{this.props.item.vicinity}</Text>
                        </View>
                    </View>
                    <View style={styles.View_3}>
                        <View style={styles.View_2}>
                            <View style={styles.View_distance}>
                                {this.props.item.distance ? <Text style={styles.Text_distance}>{this.props.item.distance}</Text> : <Text style={styles.Text_distance}>...</Text>}
                                <Text style={styles.Text_1}>Khoảng cách</Text>
                            </View>
                            <View style={styles.View_distance}>
                                {this.props.item.duration ? <Text style={styles.Text_distance}>{this.props.item.duration}</Text> : <Text style={styles.Text_distance}>...</Text>}
                                <Text style={styles.Text_1}>Ước tính</Text>
                            </View>
                        </View>
                        <View style={styles.View_1}>
                            <TouchableOpacity onPress={()=>{
                                this.update();
                                console.log(this.props.item.distance)
                                }}
                                style={styles.Direction}>
                            <Text style={styles.Text_2}>Đi đến đây  </Text>
                            <Image source={require('../../assets/images/turn-right.png')}/>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}