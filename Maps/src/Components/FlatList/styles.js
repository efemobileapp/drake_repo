import {StyleSheet} from 'react-native';

const styles=StyleSheet.create({
    container:{
        flex:1,
    },
    View_FlatList_Item:{
        backgroundColor: 'white',
        flex:1,
        flexDirection: 'column',
        // alignItems: 'center',
        width: 280,
        borderWidth: 0.5,
        borderRadius:15,
        margin: 5
    },
    View_name:{
        flex:1,
        flexDirection:'row',
        margin:20
    },
    View_location:{
        flex:3,
        marginLeft:20
    },
    View_3:{
        flex:1,
        // backgroundColor:'#F5F5F5',
        flexDirection:'column'
    },
    View_2:{
        flex:3,
        flexDirection:'row',
    },
    View_distance:{
        flex:1,
        borderWidth:0.3,
        borderColor:'grey',
    },
    View_1:{
        flex:2,
        flexDirection:'row',
        alignItems:'center',
        justifyContent: 'center',
        // padding:10
    },
    Direction:{
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        flexDirection:'row',
        alignItems:'center',
    },
    Text_2:{
        fontSize:20,
        fontWeight:'bold',
        marginLeft:70,
    },
    Text_distance:{
        margin:20,
        marginBottom:0,
        marginTop:15,
        fontSize:21,
        fontWeight:'bold',
    },
    Text_1:{
        margin:20,
        marginTop: 0,                               
        fontSize:15,   
        color: '#9C9C9C',                     
    },
    Text_name:{
        fontSize:19,
        fontWeight:'bold'
    },
    Text_Location:{
        fontSize:16,
        fontWeight:'bold',
        margin:5,
        color:'#9C9C9C'
    },
})

export default styles;