import { Text, View, StyleSheet, Linking, TouchableOpacity, Alert, Platform } from 'react-native';
export default openGps = (latitude,longitude) => {
    var scheme = Platform.OS === 'ios' ? 'maps:' : 'geo:'
    var url = "https://www.google.com/maps/dir/?api=1&travelmode=driving&dir_action=navigate&destination="+latitude+","+longitude;
    this.openExternalApp(url)
  }

openExternalApp = (url) => {
  Linking.canOpenURL(url).then(supported => {
  if (!supported) {
      console.log('Can\'t handle url: ' + url);
  } else {
      return Linking.openURL(url);
  }
}).catch(err => console.error('An error occurred', err)); 
}