import React, {Component} from 'react';
import {
    View, Dimensions, Image, Text, FlatList
} from 'react-native';
import Mapview, {PROVIDER_GOOGLE} from 'react-native-maps';
import Geocoder from 'react-native-geocoder';
import MapStyle from './Mapstyle';
import styles from './styles';
import {Gas_Station_List_data} from '../../assets/Data/Gas_Station_List_data';
import GasStationList from '../FlatList/index';
import GeocoderIndex from '../View_Geocoder/index';

let { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.01;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

export default class Geolocation extends Component{

    constructor(props){
        super(props);
        this.state={
            region:{
                latitude:0,
                longitude:0,
                latitudeDelta:LATITUDE_DELTA,
                longitudeDelta:LONGITUDE_DELTA
            },
            marker:{
                latitude:0,
                longitude:0,
            },
            mylocation:{
                city_name:'',
                street_name:'',
            }
        }
    }

    async componentWillMount() {
        await navigator.geolocation.getCurrentPosition( //Lay vi tri hien tai
            async (position) => {
                await this.setState({
                    region: {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                        latitudeDelta: LATITUDE_DELTA,
                        longitudeDelta: LONGITUDE_DELTA,
                    },
                    marker:{
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                    }
                });
            },
            (error) => alert(error.message),
            { enableHighAccuracy: false, timeout: 10000},
        );        
        this.watchID = await navigator.geolocation.watchPosition(async position => {
                await this.setState({
                    region: {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                        latitudeDelta: LATITUDE_DELTA,
                        longitudeDelta: LONGITUDE_DELTA,
                    },
                })
            });
    }
    //Get value geolocation
    async getData(){
        // alert(JSON.stringify(this.state.marker))
        Geocoder.fallbackToGoogle('AIzaSyAYPV3ycnVtKA9cSPHO9tmrwZ7pemsW64I');           
        
        try {

            const res = await Geocoder.geocodePosition({lat: this.state.marker.latitude, lng: this.state.marker.longitude});        
            
            await this.setState({
                mylocation:{
                    city_name : res[0].adminArea,
                    street_name : res[0].streetName,
                }
            });
            
        }
        catch(err) {            
            alert(err);
        }
    }

    componentWillUnmount() {
        navigator.geolocation.clearWatch(this.watchID);
    }

    render(){
        // alert(JSON.stringify(this.state.marker))
        return(
            <View style={styles.container}>
                <View style={styles.View_Maps}>
                    <Mapview style={styles.container}
                            customMapStyle={MapStyle}
                            region={this.state.region}
                            showsUserLocation={true}
                            showsMyLocationButton={false}
                            onPress={()=>{this.getData()
                            }}
                            >
                        <Mapview.Marker
                            coordinate={this.state.marker}
                            image source={require('../../assets/images/car.png')}>
                        </Mapview.Marker>
                    </Mapview>
                </View>
                <GeocoderIndex {...this.state}/>
                <View style={styles.View_FlatList}>
                        <FlatList
                            style={{opacity:1}}
                            horizontal={true}
                            data={Gas_Station_List_data}
                            renderItem={({item,index})=>{
                                return(
                                    <GasStationList item={item} index={index} parentFlatList={this}/>
                                );
                            }}
                            keyExtractor={(item,index)=>item.name}
                        >

                        </FlatList>
                </View>
            </View>
        );
    }
}