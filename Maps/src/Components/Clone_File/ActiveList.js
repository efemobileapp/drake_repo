import React, {Component} from 'react';
import {
    View, Image, Text, TouchableOpacity,
} from 'react-native';
import styles from './styles';

export default class GasStationList extends Component{ //Lớp tạo giao diện FlatList
    render(){
        return(
            <View style={styles.View_FlatList_Item}>
                <View style={styles.container}>
                    <View style={styles.View_name}>
                        <Image source={require('../../assets/images/gas-server-32.png')}/>
                        <Text style={styles.Text_name}>  {this.props.item.name}</Text>
                    </View>
                    <View style={styles.View_location}>
                        <Text style={styles.Text_Location}>{this.props.item.location}</Text>
                    </View>
                </View>
                <View style={styles.View_3}>
                    <View style={styles.View_2}>
                        <View style={styles.View_distance}>
                            <Text style={styles.Text_distance}>{this.props.item.distance}</Text>
                            <Text style={styles.Text_1}>Khoảng cách</Text>
                        </View>
                        <View style={styles.View_distance}>
                        <Text style={styles.Text_distance}>{this.props.item.time}</Text>
                            <Text style={styles.Text_1}>Ước tính</Text>
                        </View>
                    </View>
                    <View style={styles.View_1}>
                        <TouchableOpacity onPress={()=>{
                            alert("Đang tìm đường...");
                            }}
                            style={styles.Direction}>
                        </TouchableOpacity>
                        <Text style={styles.Text_2}>Đi đến đây  </Text>
                        <Image source={require('../../assets/images/turn-right.png')}/>
                    </View>
                </View>
            </View>
        );
    }
}