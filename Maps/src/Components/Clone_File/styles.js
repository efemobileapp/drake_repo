import {StyleSheet} from 'react-native';

const styles=StyleSheet.create({
    container:{
        flex:1,
    },
    container_background:{
        flex:1,
        backgroundColor:'#FFFFFF',
    },
    View_Maps:{
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
    },
    View_city:{
        position:'absolute',
        bottom:310,
        left:15,
    },
    View_streetname:{
        position:'absolute',
        bottom:330,
        left:15,
    },
    View_vtht:{
        position:'absolute',
        bottom:350,
        left:15,
    },
    View_FlatList:{
        height:280,
        position:'absolute',
        bottom:15,
        left:15
    },
    Text_city:{
        fontSize:30,
        fontWeight:'bold',
        color: '#FFFFFF',
        margin:3,
        marginLeft:15
    },
    Text_streetname:{
        fontSize:20,
        color: '#FFFFFF',
        margin:3,
        marginLeft:15
    },
    Text_vtht:{
        fontSize:18,
        fontWeight:'bold',
        color: '#FFFFFF',
        margin:3,
        marginLeft:15
    },
})

export default styles;