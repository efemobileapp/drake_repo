import React, {Component} from 'react';
import {
    View,Text,Image,Dimensions, Animated
} from 'react-native';
import MapView, { Marker, Callout} from 'react-native-maps';
import Mapstyle from './Mapstyle';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/Ionicons';
import Geocoder from 'react-native-geocoder';
import MapViewDirections from 'react-native-maps-directions';
import RNBottomActionSheet from 'react-native-bottom-action-sheet';
import GasStationList from '../FlatList/index';
import GeocoderIndex from '../View_Geocoder/index';
import GOOGLE_MAPS_APIKEY from '../../config';
// import { keys } from '../../config';
import GOOGLE_MAPS_APIKEY from '../../config/index';
import map_navigation from '../MapNavigation';

import styles from './styles';

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.00922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

export default class googleMapsTest extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            region:{
                lat: null,
                long: null,
            },

            keyword_1:'',

            places: null,

            isHidden: false,

            data_1:[],

            distancex:{
                distance: null,
                duration: null
            },
            destinationx:{
                latDestination: null,
                longDestination: null
            },
            destination:{
                latitude:null,
                longitude: null
            },
            mylocation:{
                city_name:'',
                street_name:'',
            },
            data_for_marker: [],
            data_fit_all:[],
        }
        this.updateDistance=this.updateDistance.bind(this);
    }

    async componentWillMount(){
        // console.log( keys )
        // console.log( keys.GOOGLE_MAPS_APIKEY )

        await navigator.geolocation.getCurrentPosition(async(position)=>{
            const lat = position.coords.latitude;
            const long = position.coords.longitude;
            console.log( position.coords )
            await this.setState({region:{lat, long}})
        },
        (error) => alert(JSON.stringify(error)),
        {enableHighAccuracy: false, timeout: 10000, maximumAge:5000});

        this.index = 0;
        this.animation = new Animated.Value(0);

    }

    componentForAnimated(){
        this.animation.addListener(({ value }) => {
            let index = Math.floor(value / 280 + 0.5); // animate 30% away from landing on the next item
            if (index >= this.state.data_for_marker.length) {
              index = this.state.data_for_marker.length - 1;
            }
            if (index <= 0) {
              index = 0;
            }
      
            clearTimeout(this.regionTimeout);
            this.regionTimeout = setTimeout(() => {
              if (this.index !== index) {
                this.index = index;
                const { coordinate } = this.state.data_for_marker[index];
                this.map.animateToRegion(
                  {
                    ...coordinate,
                    latitudeDelta: LATITUDE_DELTA,
                    longitudeDelta: LONGITUDE_DELTA,
                  },
                  350
                );
              }
            }, 10);
          });
    }

    async getPlaces(){
        await this.setState({data_for_marker: []})
        const url = this.getUrlWithParameters(this.state.region.lat, this.state.region.long, 1500, 'name', ''+this.state.keyword_1, GOOGLE_MAPS_APIKEY)
        await fetch(url)
            .then((data) => data.json())
            .then(async (res) => {

                await this.setState({data_1:[], data_fit_all:[]})
                console.log(res)
                const arrayMarkers=[];

                res.results.map(async (element, i)=>{

                    var data_2={};
                    arrayMarkers.push(
                        <Marker
                            key={i}
                            coordinate={{
                                latitude: element.geometry.location.lat,
                                longitude: element.geometry.location.lng,
                            }}
                        >
                            <Callout>
                                <View>
                                    <Text>{element.vicinity}</Text>
                                </View>
                            </Callout>
                        </Marker>
                    )
                    this.setState({destinationx:{
                        latDestination: res.results[i].geometry.location.lat,
                        longDestination: res.results[i].geometry.location.lng,
                    }})

                    data_2.latitude = this.state.destinationx.latDestination;
                    data_2.longitude = this.state.destinationx.longDestination;

                    this.state.data_for_marker.push({
                        coordinate: data_2
                    }); 
                    this.state.data_fit_all.push(data_2);

                    await this.getDistance(i,res);

                })
                await this.setState({places: arrayMarkers}) 
        })
        this.fitAllMarkers();
    }

    //Get value geolocation
    async getData(){
        // alert(JSON.stringify(this.state.marker))
        Geocoder.fallbackToGoogle(GOOGLE_MAPS_APIKEY);
        
        try {

            const res = await Geocoder.geocodePosition({lat: this.state.region.lat, lng: this.state.region.long});        
            
            await this.setState({
                mylocation:{
                    city_name : res[0].adminArea,
                    street_name : res[0].streetName,
                }
            });
            
        }
        catch(err) {            
            alert(err);
        }
    }

    async getDistance(i,ret){
        const url = this.getURLMapDirections(this.state.region.lat, this.state.region.long,this.state.destinationx.latDestination, this.state.destinationx.longDestination)
        await fetch(url)
            .then((data)=>data.json())
            .then(async res =>{
                await this.setState({
                    distancex:{
                        distance: res.routes[0].legs[0].distance.text,
                        duration: res.routes[0].legs[0].duration.text,
                    }
                })
                this.state.data_1[i] = ret.results[i];
                this.state.data_1[i].distance = res.routes[0].legs[0].distance.text; 
                this.state.data_1[i].duration = res.routes[0].legs[0].duration.text; 

            })
    }

    getUrlWithParameters(lat, long, radius, type, keyword, API){
        const url="https://maps.googleapis.com/maps/api/place/nearbysearch/json?";
        const location='location='+lat+','+long+'&radius='+radius;
        const typeData= '&type='+type;
        const key_word = '&keyword='+keyword;
        const key = '&key='+API;
        return ''+url+location+typeData+key_word+key;
    }

    getURLMapDirections(latO, longO, latD, longD){
        const url="https://maps.googleapis.com/maps/api/directions/json?";
        const originm = "origin="+latO+","+longO;
        const destinatiom = "&destination="+latD+","+longD;
        const key = "&key="+GOOGLE_MAPS_APIKEY;
        return ""+url+originm+destinatiom+key;
    }

    updateDistance= async (lat, lng)=>{
        await this.setState({
            destination:{
                latitude:lat,
                longitude: lng
            },
            isHidden: false
        })
        // await this.fitPaddingForShowAlert();
        this._showAlertView();
    }

    fitPadding() {
        this.map.fitToCoordinates([{latitude: this.state.region.lat, longitude: this.state.region.long}, {latitude: this.state.destination.latitude, longitude: this.state.destination.longitude}], {
          edgePadding: { top: 100, right: 100, bottom: 1000, left: 100 },
          animated: true,
        });
    }

    fitPaddingForShowAlert() {
        this.map.fitToCoordinates([{latitude: this.state.region.lat, longitude: this.state.region.long}, {latitude: this.state.destination.latitude, longitude: this.state.destination.longitude}], {
          edgePadding: { top: 100, right: 100, bottom: 100, left: 100 },
          animated: true,
        });
    }
    
    fitAllMarkers() {
        this.map.fitToCoordinates(this.state.data_fit_all, {
          edgePadding: { top: 100, right: 100, bottom: 1000, left: 100 },
          animated: true,
        });
    }

    async onPress_Gastation(){
        await this.setState({
            keyword_1: 'petro',
            isHidden: true,
            destination:{
                latitude:null,
                longitude: null
            }
        })
        await this.componentForAnimated();
        this.getPlaces();
        this.getData();
    }

    _showAlertView = () => {
        let AlertView = RNBottomActionSheet.AlertView
        AlertView.Show({
          title: "Quản lý lộ trình!",
          message: "Hiển thị lộ trình chi tiết.",
          positiveText: "Bắt đầu",
          positiveBackgroundColor: "#eeffee",
          positiveTextColor: "#006500",
          negativeText: "Hủy",
          negativeBackgroundColor: "#ffebeb",
          negativeTextColor: "#760000",
          theme: 'light',
          onPositive: () => {
            // this.setState({ isVisible: true })
            map_navigation(this.state.destination.latitude,this.state.destination.longitude)
          },
          onNegative: () => {
            this.setState({
                isHidden: true
            })
            this.fitPadding();
          }
        })
    }



    render(){
        const interpolations = this.state.data_for_marker.map((marker, index) => {
            const inputRange = [
              (index - 1) * 300,
              index * 300,
              ((index + 1) * 300),
            ];
            const scale = this.animation.interpolate({
                inputRange,
                outputRange: [1, 2.5, 1],
                extrapolate: "clamp",
              });
            const opacity = this.animation.interpolate({
                inputRange,
                outputRange: [0.35, 1, 0.35],
                extrapolate: "clamp",
              });
              return { scale, opacity };
          });      

        return(
            <View style={styles.View_All_Map}>
                {this.state.region.lat ? <MapView
                    ref={ref => this.map=ref}
                    style={{flex:1}}
                    provider={MapView.PROVIDER_GOOGLE}
                    showsUserLocation={true}
                    customMapStyle={Mapstyle}
                    initialRegion={{
                        latitude: this.state.region.lat,
                        longitude: this.state.region.long,
                        latitudeDelta: LATITUDE_DELTA,
                        longitudeDelta: LONGITUDE_DELTA
                    }}
                    onPress={()=>{this.setState({isHidden: false})}}
                    >
                    {this.state.destination.latitude ?<MapViewDirections
                        origin={{latitude: this.state.region.lat, longitude: this.state.region.long}}
                        destination={this.state.destination}
                        apikey={GOOGLE_MAPS_APIKEY}
                        strokeWidth={3}
                        strokeColor="hotpink"
                    />:null}
                    <Marker
                        coordinate={{
                            latitude: this.state.region.lat,
                            longitude: this.state.region.long,
                        }}
                    >
                        <View>
                            <Image source={require('../../assets/images/car.png')}/>
                        </View>
                    </Marker>
                    {this.state.data_for_marker.map((marker, index) => {
                        const scaleStyle = {
                        transform: [{
                            scale: interpolations[index].scale,
                        },],
                        };
                        const opacityStyle = {
                        opacity: interpolations[index].opacity,
                        };
                        return (
                        <MapView.Marker key={index} coordinate={marker.coordinate}>
                            <Animated.View /*style={[styles.markerWrap, opacityStyle]}*/>
                                <Animated.View /*style={[styles.ring, scaleStyle]}*/ />
                                <View /*style={styles.marker}*/ />
                            </Animated.View>
                        </MapView.Marker>
                        );
                    })}
                </MapView>: null}
                {this.state.isHidden ? <GeocoderIndex {...this.state}/>:null}
                <View style={{flex:1, position:'absolute', top:0,bottom:0,left:0,right:0}}>
                    <ActionButton buttonColor="rgba(231,76,60,1)">
                        <ActionButton.Item buttonColor='#9b59b6' title="Gasstation" onPress={() => {this.onPress_Gastation()}}>
                        <Icon name="md-alarm" style={styles.actionButtonIcon} />
                        </ActionButton.Item>
                    </ActionButton>
                </View>
                {this.state.isHidden ? 
                <Animated.ScrollView
                    horizontal
                    scrollEventThrottle={1}
                    showsHorizontalScrollIndicator={false}
                    snapToInterval={290}
                    onScroll={Animated.event(
                    [{
                        nativeEvent: {
                            contentOffset: {
                            x: this.animation,
                            },
                        },
                    },],
                    { useNativeDriver: true }
                    )}
                    style={styles.scrollView}
                    contentContainerStyle={styles.endPadding}
                >
                    {this.state.data_1.map((marker, index) => (
                    <GasStationList item={marker} key={index} updateDistance={this.updateDistance} fitPadding={this.fitPadding} {...this.state}/>
                    ))}
                </Animated.ScrollView>
                :null} 
            </View>
        );
    }
}
