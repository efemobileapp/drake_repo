import {Dimensions} from 'react-native'

import {StyleSheet} from 'react-native';
const { width, height } = Dimensions.get('window');

const styles=StyleSheet.create({
    container:{
        flex:1,
    },
    View_FlatList_Item:{
        backgroundColor: 'white',
        flex:1,
        flexDirection: 'column',
        // alignItems: 'center',
        width: 280,
        borderWidth: 0.5,
        borderRadius:15,
        margin: 5
    },
    View_All_Map:{
        position:'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
    },
    View_name:{
        flex:1,
        flexDirection:'row',
        margin:20
    },
    View_location:{
        flex:3,
        marginLeft:20
    },
    View_3:{
        flex:1,
        // backgroundColor:'#F5F5F5',
        flexDirection:'column'
    },
    View_2:{
        flex:3,
        flexDirection:'row',
    },
    View_distance:{
        flex:1,
        borderWidth:0.3,
        borderColor:'grey',
    },
    View_1:{
        flex:2,
        flexDirection:'row',
        alignItems:'center',
        // padding:10
    },
    Direction:{
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        flexDirection:'row',
        alignItems:'center',
    },
    Text_2:{
        fontSize:20,
        fontWeight:'bold',
        marginLeft:70,
        // marginTop:5,
    },
    Text_distance:{
        margin:20,
        marginBottom:0,
        marginTop:15,
        fontSize:21,
        fontWeight:'bold',
    },
    Text_1:{
        margin:20,
        marginTop: 0,                               
        fontSize:15,   
        color: '#9C9C9C',                     
    },
    Text_name:{
        marginRight:5,
        fontSize:20,
        fontWeight:'bold'
    },
    Text_Location:{
        fontSize:18,
        fontWeight:'bold',
        color:'#9C9C9C'
    },
    actionButtonIcon: {
        fontSize: 20,
        height: 22,
        color: 'white',
      },
      markerWrap: {
          alignItems: "center",
          justifyContent: "center",
        },
      marker: {
          width: 8,
          height: 8,
          borderRadius: 4,
          backgroundColor: "rgba(255, 0, 0, 0.9)",
        },
      ring: {
          width: 24,
          height: 24,
          borderRadius: 12,
          backgroundColor: "rgba(255, 0, 0, 0.3)",
          position: "absolute",
          borderWidth: 1,
          borderColor: "rgba(130,4,150, 0.5)",
        },
      scrollView: {
          height:300,
          position: "absolute",
          bottom: 0,
          left: 0,
          right: 0,
          paddingVertical: 10,
        },
      endPadding: {
          paddingRight: width - 260,
        },
})

export default styles;