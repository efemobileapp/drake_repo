import React, {Component} from 'react';
import {
    View, Text, TextInput,Keyboard, TouchableOpacity
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import styles from './styles';

export default class EditTask extends Component{

    constructor(props){
        super(props);
        this.state={
            index: this.props.navigation.state.params.indexKey,
            textTitle: null,
            textContent: null,
            time:null,
        }
    }

    componentWillMount(){
        this.setState({
            index: this.props.navigation.state.params.indexKey,
            textTitle: this.props.navigation.state.params.dataStask[this.state.index].title,
            textContent: this.props.navigation.state.params.dataStask[this.state.index].content,
        })
    }

    _textTitle = (value)=>{
        this.setState({
            textTitle: value
        })
    }

    _textContent = (value)=>{
        this.setState({
            textContent: value
        })
    }

    _checkTime=(i) =>{
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }

    _ShowCurrentDate=async()=>{
 
        var date = new Date().getDate();
        var month = new Date().getMonth() + 1;
        var year = new Date().getFullYear();
        var hour = new Date().getHours();
        var minutes = new Date().getMinutes();

        date = this._checkTime(date);
        month = this._checkTime(month);
        hour = this._checkTime(hour);
        minutes = this._checkTime(minutes);
   
        await this.setState({
            time: 'Đã sửa: '+hour + ' : ' +minutes+ ' | ' +date+ '-' + month + '-' + year,
        })

    }

    _EditTask=async()=>{
        await this._ShowCurrentDate();
        var timeBefore=this.props.navigation.state.params.dataStask[this.state.index].time;
        var item = {title:this.state.textTitle,content:this.state.textContent,time:timeBefore+' --- '+this.state.time}
        this.props.navigation.state.params.dataStask.splice(this.state.index, 1, item)
    }

    _ClickSaveChange= async ()=>{
        await this._EditTask();
        this.props.navigation.navigate('Home')
    }

    render(){
        return(
            <KeyboardAwareScrollView>
                <View style={styles.container}>
                    <Text style={styles.Text_Tieude}>Tiêu đề:</Text>
                    <TextInput 
                        style={styles.TextInput_Title}
                        maxLength = {100}
                        multiline = {true}
                        numberOfLines= {3}
                        placeholder = 'Nhập tiêu đề...'
                        autoCapitalize = 'sentences'
                        onChangeText = {(value) => this._textTitle(value)}
                    >
                        {this.props.navigation.state.params.dataStask[this.state.index].title}
                    </TextInput>
                    <Text style={styles.Text_Tieude}>Nội dung:</Text>
                    <TextInput 
                        style={styles.TextInput_content}
                        maxLength = {100}
                        multiline = {true}
                        numberOfLines= {3}
                        placeholder = 'Nhập nội dung...'
                        autoCapitalize = 'sentences'
                        onChangeText = {(value) => this._textContent(value)}
                    >
                        {this.props.navigation.state.params.dataStask[this.state.index].content}
                    </TextInput>
                    <TouchableOpacity 
                        style={styles.click_save}
                        onPress={()=>{this._ClickSaveChange()}}>
                        <Text style={styles.Text_save}>Lưu thay đổi</Text>
                    </TouchableOpacity>
                </View>
            </KeyboardAwareScrollView>
        );
    }
}