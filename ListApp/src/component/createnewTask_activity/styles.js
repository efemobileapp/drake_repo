import {StyleSheet,Dimensions} from 'react-native';
const { width, height } = Dimensions.get('window');
const styles=StyleSheet.create({
    container:{
        flex: 1
    },
    Text_Tieude:{
        marginTop: 20,
        marginLeft: 20,
        fontSize: 18,
        fontWeight: 'bold',
        color: '#FFCC66',  
    },
    TextInput_Title:{
        width: width-40,
        alignSelf:'baseline',
        alignItems:'stretch',
        borderWidth: 0.5,
        borderRadius:5,
        margin: 20,
        fontSize: 20,
        padding: 10,
        textAlignVertical: "top"
    },
    TextInput_content:{
        width: width-40,
        height: height/3.5,
        borderWidth: 0.5,
        borderRadius:5,
        margin: 20,
        fontSize: 20,
        padding: 10,
        textAlignVertical: "top"
    },
    click_save:{
        margin: 20,
        marginTop: height - 640,
        height: 60,
        backgroundColor: '#FFCC66',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 3
    },
    Text_save:{
        fontSize: 25,
        fontWeight: 'bold',
        color: '#FFFFFF',  
    }
})

export default styles;