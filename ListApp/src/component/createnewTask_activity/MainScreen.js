import React, {Component} from 'react';
import {
    View, Text, TextInput,Keyboard, TouchableOpacity
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import {DataTaskForScrollView} from '../../assets/data/DataTaskForScrollView';
import styles from './styles';

export default class CreateNewTask extends Component {

    constructor(props){
        super(props);
        this.state={
            textTitle: null,
            textContent: null,
            time: null,
        }
    }

    static navigationOptions = {
        headerTitle : 'Create New Task',
    }

    _textTitle = (value)=>{
        this.setState({
            textTitle: value
        })
    }

    _textContent = (value)=>{
        this.setState({
            textContent: value
        })
    }

    _checkTime=(i) =>{
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }

    _ShowCurrentDate=async()=>{
 
        var date = new Date().getDate();
        var month = new Date().getMonth() + 1;
        var year = new Date().getFullYear();
        var hour = new Date().getHours();
        var minutes = new Date().getMinutes();

        date = this._checkTime(date);
        month = this._checkTime(month);
        hour = this._checkTime(hour);
        minutes = this._checkTime(minutes);
   
        await this.setState({
            time: 'Đã tạo: '+hour + ' : ' +minutes+ ' | ' +date+ '-' + month + '-' + year,
        })

    }

    _PustToData= async()=>{
        await this._ShowCurrentDate();
        var item = {title:this.state.textTitle,content:this.state.textContent,time:this.state.time}
        this.props.navigation.state.params.dataStask.unshift(item)
    }

    _ClickSave = async ()=>{
        await this._PustToData();
        this.props.navigation.navigate('Home')
        console.log(this.props.navigation)
    }

    render(){
        return(
            <KeyboardAwareScrollView>
                <View style={styles.container}>
                    <Text style={styles.Text_Tieude}>Tiêu đề:</Text>
                    <TextInput 
                        style={styles.TextInput_Title}
                        maxLength = {100}
                        multiline = {true}
                        numberOfLines= {3}
                        placeholder = 'Nhập tiêu đề...'
                        autoCapitalize = 'sentences'
                        onChangeText = {(value) => this._textTitle(value)}
                    />
                    <Text style={styles.Text_Tieude}>Nội dung:</Text>
                    <TextInput 
                        style={styles.TextInput_content}
                        maxLength = {100}
                        multiline = {true}
                        numberOfLines= {3}
                        placeholder = 'Nhập nội dung...'
                        autoCapitalize = 'sentences'
                        onChangeText = {(value) => this._textContent(value)}
                    />
                    <TouchableOpacity 
                        style={styles.click_save}
                        onPress={()=>{this._ClickSave()}}>
                        <Text style={styles.Text_save}>Lưu</Text>
                    </TouchableOpacity>
                </View>
            </KeyboardAwareScrollView>
        );
    }
}