import React, {Component} from 'react';
import {
    View, Text, TouchableHighlight
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import styles from './styles';

export default class CreateNewTask extends Component {

    constructor(props){
        super(props)
        this.state={
            checked: false,
        }
    }

    componentDidMount(){
        this.props.onRef(this)
    }

    componentWillUnmount(){
        this.props.onRef(undefined)
    }

    async _onPressButton(index,item) {
        
        await this.setState({
            checked: !this.state.checked
        });
        let i = this.props.checked.indexOf(index);
        {this.state.checked ?
            this.props.checked.unshift(index)
        : this.props.checked.splice(i,1);}
        this.props.onPressRow()
    }

    _onLongPressButton(){
        this.props.checkList();
    }

    _checkAll=()=>{
        this.setState({checked:!this.state.checked})
    }

    render(){
        return(
            <View style={styles.container}>
                {this.props.item ? 
                    <TouchableHighlight style={styles.View_scroll} onPress={()=>{this._onPressButton(this.props.index,this.props.item)}} onLongPress={()=>{this._onLongPressButton()}} underlayColor='white'>
                        <View style={styles.container}>
                            <View style={styles.View_title}>
                                <Text style={styles.Text_title}>{this.props.item.title}</Text>
                            </View>
                            <View style={styles.View_content}>
                                <Text style={styles.Text_content} numberOfLines={2}>{this.props.item.content}</Text>
                            </View>
                            <View style={styles.View_time}>
                                <Text style={styles.Text_time}>{this.props.item.time}</Text>
                            </View>
                            <Icon name='md-checkmark'
                                  size={20}
                                  color='white'
                                  style={{position:'absolute', right:10, top:10, backgroundColor:this.state.checked ? 'rgba(236, 158, 56, 0.9)' : 'white', height: 30, width:30, borderRadius:40, borderWidth:0.01, padding:5,paddingLeft:8}}/>
                        </View> 
                    </TouchableHighlight>
                    : <View style={styles.View_null}>
                        <Text style={styles.Text_null}>Không có nhiệm vụ nào!</Text>
                    </View>
               }
            </View>
        );
    }
}