import React, {Component} from 'react';
import {
    View, Text, TouchableHighlight
} from 'react-native';

import styles from './styles';

export default class CreateNewTask extends Component {

    constructor(props){
        super(props)
        this.state={
            checked: false
        }
    }

    _onPressButton=(index,item)=> {
        // alert(index)
        this.props.editTask(index);
        
    }
    _onLongPressButton(){
        this.props.checkList();
    }
    render(){
        return(
            <View style={styles.container}>
                {this.props.item ? 
                    <TouchableHighlight style={styles.View_scroll} onPress={()=>{this._onPressButton(this.props.index,this.props.item)}} onLongPress={()=>{this._onLongPressButton()}} underlayColor="white">
                        <View style={styles.container}>
                            <View style={styles.View_title}>
                                <Text style={styles.Text_title}>{this.props.item.title}</Text>
                            </View>
                            <View style={styles.View_content}>
                                <Text style={styles.Text_content} numberOfLines={2}>{this.props.item.content}</Text>
                            </View>
                            <View style={styles.View_time}>
                                <Text style={styles.Text_time}>{this.props.item.time}</Text>
                            </View>
                        </View> 
                    </TouchableHighlight>
                    : <View style={styles.View_null}>
                        <Text style={styles.Text_null}>Không có nhiệm vụ nào!</Text>
                    </View>
               }
            </View>
        );
    }
}