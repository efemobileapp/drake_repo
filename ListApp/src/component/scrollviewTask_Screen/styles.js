import {StyleSheet, Dimensions} from 'react-native';
const { width, height } = Dimensions.get('window');
const styles=StyleSheet.create({
    container:{
        flex: 1
    },
    View_scroll:{
        backgroundColor: 'white',
        flex:1,
        flexDirection: 'column',
        // alignItems: 'center',
        width: width-10,
        alignSelf:'baseline',
        borderWidth: 0.5,
        borderRadius:10,
        borderColor: '#BBBBBB',
        margin: 5
    },
    View_null:{
        alignItems:'center',
        justifyContent: 'center',
    },
    View_title:{
        flex:1,
        alignSelf:'baseline',
        paddingTop:10,
        paddingLeft:10,
        paddingRight:10
    },
    View_content:{
        flex:1,
        alignSelf:'baseline',
        paddingLeft:10,
        paddingRight:10,
        paddingBottom: 10,
        // paddingTop: 5
    },
    View_time:{
        flex:1,
    },
    Text_title:{
        fontSize:20,
        fontWeight:'bold',
    },
    Text_content:{
        fontSize: 14
    },
    Text_time:{
        fontSize: 12,
        justifyContent:'flex-end',
        alignSelf:'flex-end',
        marginRight:5,
        marginBottom:5,
        color: '#999999'
    },
    Text_null:{
        fontSize: 20,
    }
})

export default styles;