import React, {Component} from 'react';
import {
    View, Text, FlatList, RefreshControl, TouchableOpacity
} from 'react-native';
import { createStackNavigator, createAppContainer } from "react-navigation";
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/Ionicons';
import { CheckBox } from 'react-native-elements';

import styles from './styles';
import NewTaskScreen from '../createnewTask_activity';
import EditTaskScreen from '../editTask_activity'
import ScrollTask from '../scrollviewTask_Screen';
import FlatlistCheckbox from '../checkboxFlatlist'
import { DataTaskForScrollView } from '../../assets/data/DataTaskForScrollView';

class MainActivity extends Component {

    static navigationOptions = {
        header:null,
        headerTitle : 'Create to do List App'
    }

    constructor(props){
        super(props);
        this.state={
            dataStask:[{title:'CHinh', content:'ks', time: 'js'}],
            indexKey: null,
            checked:[],
            checkforchild: false,
            refreshing: false,
            isVisibleCheckbox: false,
            isVisibleNomarl: true,
        }
    }

    _clickToMakeNewTask = () => {
        this.props.navigation.navigate('CreateNewTask',{...this.state});
    }

    _editTask = async (index) => {
        await this.setState({
            indexKey: index
        })
        this.props.navigation.navigate('EditTask',{...this.state})
    }

    _getParam=()=>{
        this.setState({
            refreshing:false,
            dataStask:this.state.dataStask
        })
    }

    _checkList=()=>{
        this.setState({isVisibleCheckbox: !this.state.isVisibleCheckbox, isVisibleNomarl:!this.state.isVisibleNomarl, checked:[]})
    }

    onRefresh=()=>{
        this.setState({refreshing:true})
        this._getParam()
    }

    _onPressDeleteRow=()=>{
        this.state.checked.sort()
        this.state.checked.reverse();
        for(var i in this.state.checked){
            var number=Number(this.state.checked[i]);
            this.state.dataStask.splice(number,1)
        }
        this.setState({dataStask: this.state.dataStask, checked:[]})
    }

    _onPressRow=()=>{
        this.setState({
            checked: this.state.checked
        })
    }

    _onPressCheckAll=async()=>{
        if(this.state.checked.length<this.state.dataStask.length){
            await this.setState({checked:[]})
            for(var i in this.state.dataStask){
                this.state.checked.push(i)
            }
        }
        else{
            await this.setState({checked:[]})
        }
        this.setState({checked: this.state.checked})
        this.child._checkAll()
    }

    render(){
        return(
            <View style={styles.container}>
                <View style={styles.container}>
                    <View style={styles.headerStyle}>
                        <Text style={styles.TextHeader}>Create to do List App</Text>
                    </View>
                    <View style={styles.View_row}></View>
                    {this.state.isVisibleNomarl ? <FlatList
                        style={styles.scrollView} 
                        ref={"flatList"}
                        data={this.state.dataStask}
                        renderItem={({item, index})=>{
                                return(
                                    <ScrollTask 
                                        item={item} index={index} parentFlatList={this} {...this.state}
                                        checkList={this._checkList} 
                                        editTask={this._editTask} 
                                    />
                                );
                            }
                        }
                        keyExtractor = {(item, index) => item.title}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={this.onRefresh}
                            />
                        }
                    />:null}
                    <ActionButton buttonColor="rgba(236, 158, 56, 0.9)">
                        <ActionButton.Item buttonColor='rgba(236, 158, 56, 0.9)' title="New Task" onPress={() => this._clickToMakeNewTask()}>
                            <Icon name="md-create" style={styles.actionButtonIcon} />
                        </ActionButton.Item>
                    </ActionButton>
                </View>
                {this.state.isVisibleCheckbox?<View style={styles.container_checkbox}>
                    <View style={styles.headerStyle}>
                        <Text style={styles.TextHeader}>Create to do List App</Text>
                    </View>
                    <View style={styles.View_row}></View>
                    {this.state.dataStask ? <FlatList
                        style={styles.scrollView} 
                        ref={"flatList"}
                        data={this.state.dataStask}
                        renderItem={({item, index})=>{
                                return(
                                    <FlatlistCheckbox 
                                        item={item} index={index} parentFlatList={this} {...this.state}
                                        checkList={this._checkList} 
                                        onPressRow={this._onPressRow}
                                        onRef={ref => (this.child = ref)}              
                                    />
                                );
                            }
                        }
                        keyExtractor = {(item, index) => item.title}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={this.onRefresh}
                            />
                        }
                    />:<Text>Queen</Text>}
                    <View style={styles.View_longClick}>
                        <View style={styles.View_checkAll}>
                            <TouchableOpacity onPress={()=>{this._onPressCheckAll()}}>
                                <Icon name='md-checkmark'
                                    size={20}
                                    color='white'
                                    style={{backgroundColor:this.state.checked.length==this.state.dataStask.length ? 'rgba(236, 158, 56, 0.9)' : 'white', height:30, width:30, paddingTop:5, paddingLeft: 9, borderRadius:40, borderWidth:0.1}}/>
                                <Text style={styles.Text_all}>Tất cả</Text>
                            </TouchableOpacity>
                        </View>
                        {this.state.checked.length==0 ? <Text style={styles.Text_count}>Chọn một mục</Text> : <Text style={styles.Text_count}>{this.state.checked.length}</Text>}
                        <TouchableOpacity style={styles.Touch_delete} onPress={()=>{this._onPressDeleteRow()}}>
                            <Icon name="md-trash" style={styles.icon_delete}/>
                        </TouchableOpacity>
                    </View>
                </View>:null}
            </View>
        );
    }
}

const RootStack = createStackNavigator(
    {
        Home:           { screen: MainActivity },
        CreateNewTask:  { screen: NewTaskScreen },
        EditTask:       { screen: EditTaskScreen },
    },
    {
        initialRouteName: 'Home',
        defaultNavigationOptions: {
          headerTintColor: '#ffcc66',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        },
      }
)

export default AppContainer = createAppContainer(RootStack);