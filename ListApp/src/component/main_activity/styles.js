import {StyleSheet} from 'react-native';
const styles=StyleSheet.create({
    container:{
        flex: 1
    },
    containerStyle:{
        alignSelf:'baseline',
        backgroundColor:'#fff',
    },
    container_checkbox:{
        position: 'absolute',
        top:0,
        bottom:0,
        left:0,
        right:0
    },
    headerStyle:{
        height:57,
        padding:15
    },
    scrollView: {
        position: "absolute",
        top:60,
        bottom: 0,
        left: 0,
        right: 0,
        paddingVertical: 10,
    },
    View_longClick:{
        position: 'absolute',
        backgroundColor: '#fff',
        top: 0,
        left:0,
        right:0,
        padding: 10,
        height: 55,
        flexDirection: 'row',
    },
    View_checkAll:{
        flexDirection:'column'
    },
    View_row:{
        backgroundColor:'#AAAAAA',
        height:1,
    },
    icon_check:{
        backgroundColor: 'rgba(236, 158, 56, 0.9)',
        borderRadius:50,
        height:30,
        width:30,
        paddingTop:9,
        paddingLeft: 11
    },
    icon_delete:{
        fontSize:20,
        marginTop:7
    },
    TextHeader:{
        fontSize: 20,
        color: '#ffcc66',
        fontWeight:'bold',
        marginLeft:20
    },
    Text_count:{
        fontWeight:'bold',
        fontSize: 15,
        marginLeft:20,
        marginTop:7,
    },
    Text_all:{
        fontSize:9,
        marginLeft:2
    },
    Touch_delete:{
        marginLeft:300,
    }
})

export default styles;